import CartParser from "./CartParser";
import path from "path";

let parser;
const cart = {
  items: [
    {
      id: "3e6def17-5e87-4f27-b6b8-ae78948523a9",
      name: "Mollis consequat",
      price: 9,
      quantity: 2,
    },
    {
      id: "90cd22aa-8bcf-4510-a18d-ec14656d1f6a",
      name: "Tvoluptatem",
      price: 10.32,
      quantity: 1,
    },
    {
      id: "33c14844-8cae-4acd-91ed-6209a6c0bc31",
      name: "Scelerisque lacinia",
      price: 18.9,
      quantity: 1,
    },
    {
      id: "f089a251-a563-46ef-b27b-5c9f6dd0afd3",
      name: "Consectetur adipiscing",
      price: 28.72,
      quantity: 10,
    },
    {
      id: "0d1cbe5e-3de6-4f6a-9c53-bab32c168fbf",
      name: "Condimentum aliquet",
      price: 13.9,
      quantity: 1,
    },
  ],
  total: 348.32,
};

beforeEach(() => {
  parser = new CartParser();
});

describe("CartParser - unit tests", () => {
  test("test the total price of items added to the cart", () => {
    expect(parser.calcTotal(cart.items)).toBeCloseTo(cart.total);
  });

  describe("Validate method", () => {
    test("test validate csv headers", () => {
      const brokenProductName = "Productname",
        brokenQuantity = "Quanti",
        csv = `${brokenProductName},Price,${brokenQuantity}\nMollis consequat,9.00,2`;

      const errors = parser.validate(csv);

      const expectedProductNameError = {
          type: parser.ErrorType.HEADER,
          column: 0,
        },
        expectedQuantityError = {
          type: parser.ErrorType.HEADER,
          column: 2,
        };

      expect(errors).toEqual(
        expect.arrayContaining([
          expect.objectContaining(expectedProductNameError),
          expect.objectContaining(expectedQuantityError),
        ])
      );
    });

    test("test validate csv entity fields count", () => {
      const csvWithMissingFields =
        "Product name,Price,Quantity\nMollis consequat,9.00";

      const errors = parser.validate(csvWithMissingFields);

      expect(errors).toEqual(
        expect.arrayContaining([
          expect.objectContaining({
            type: parser.ErrorType.ROW,
            column: -1,
          }),
        ])
      );
    });

    test("test validate nonempty string", () => {
      const invalidStringCsv = "Product name,Price,Quantity\n,6,5";

      const errors = parser.validate(invalidStringCsv);

      expect(errors).toEqual(
        expect.arrayContaining([
          expect.objectContaining({
            type: parser.ErrorType.CELL,
            column: 0,
          }),
        ])
      );
    });

    test("test validate positive number", () => {
      const invalidNumberCsv =
        "Product name,Price,Quantity\nMollis consequat,,-4";

      const errors = parser.validate(invalidNumberCsv);

      expect(errors).toEqual(
        expect.arrayContaining([
          expect.objectContaining({
            type: parser.ErrorType.CELL,
            column: 2,
          }),
        ])
      );
      expect(errors.length).toBe(1);
    });

    test("test validate csv success", () => {
      const validCsv =
          "Product name,Price,Quantity\nMollis consequat,9.00,2\nTvoluptatem,10.32,1",
        errors = parser.validate(validCsv);
      expect(errors.length).toBe(0);
    });
  });

  test("parse csvLine to object successfully", () => {
    const csvLine = "Consectetur adipiscing,28.72,10";
    const parsedCsv = parser.parseLine(csvLine);
    expect(parsedCsv).toMatchObject({
      name: "Consectetur adipiscing",
      price: 28.72,
      quantity: 10,
    });
    expect(!!parsedCsv.id).toBe(true);
  });
});

describe("CartParser - integration test", () => {
  test("should parse", () => {
    const cardPath = path.join(__dirname, "../samples/cart.csv");
	const invalidCardPath = path.join(__dirname, "../samples/invalidCart.csv");
	
    expect(() => parser.parse(invalidCardPath)).toThrow("Validation failed!");

	const parsed = parser.parse(cardPath)
	expect(() => parser.parse(cardPath)).not.toThrow();
	expect(parsed.items.length).toBe(5)
  });
});
